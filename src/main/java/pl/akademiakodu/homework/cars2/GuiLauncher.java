package pl.akademiakodu.homework.cars2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.MainMenuController;

public class GuiLauncher extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    /*
        Loads and starts the view.
        1. Load FXML file using FXMLLoader.
        2. Build Scene.
        3. Sent Scene to Stage and show view.
     */

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/main_menu.fxml"));
        Parent root = fxmlLoader.load();
        MainMenuController mainMenuController = fxmlLoader.getController();
        mainMenuController.setStage(stage);

        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.show();
    }
}
