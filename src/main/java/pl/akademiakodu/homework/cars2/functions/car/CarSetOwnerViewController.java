package pl.akademiakodu.homework.cars2.functions.car;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;
import pl.akademiakodu.homework.cars2.functions.car.car_owner.CarOwner;
import pl.akademiakodu.homework.cars2.functions.car.car_owner.CarOwnerDao;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.regex.Pattern;

public class CarSetOwnerViewController {

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private DatePicker datePickerBeginDate;
    @FXML
    private DatePicker datePickerFinishDate;
    @FXML
    private TextField textFieldCarId;
    @FXML
    private TextField textFieldOwnerId;
    private Stage stage;

    @FXML
    private void buttonAddOnActionListener() {
        Pattern pattern = Pattern.compile("^[0-9]+$");

        if (!pattern.matcher(textFieldCarId.getText()).matches()) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Car ID is not a integer number.");
                anchorPane.setDisable(true);
                stageResult.show();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        if (!pattern.matcher(textFieldOwnerId.getText()).matches()) {

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Owner ID is not a integer number.");
                anchorPane.setDisable(true);
                stageResult.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (datePickerBeginDate.getValue() == null) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Begin date wrong format.");
                anchorPane.setDisable(true);
                stageResult.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

//        if (datePickerFinishDate.getValue() == null){
//            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
//            Parent root;
//            try {
//                root = fxmlLoader.load();
//                Scene scene = new Scene(root);
//                Stage stageResult = new Stage();
//                StatementViewController statementViewController = fxmlLoader.getController();
//                statementViewController.setStage(stageResult);
//                statementViewController.setAnchorPane(anchorPane);
//                stageResult.setScene(scene);
//                stageResult.setResizable(false);
//                stageResult.setAlwaysOnTop(true);
//                statementViewController.setLabelStatementText("Finish date wrong format.");
//                anchorPane.setDisable(true);
//                stageResult.show();
//            } catch (IOException e) {
//                e.printStackTrace();
//            } finally {
//                return;
//            }
//        }

        int idCar = Integer.parseInt(textFieldCarId.getText());
        int idOwner = Integer.parseInt(textFieldOwnerId.getText());
        LocalDate dateBegin = datePickerBeginDate.getValue();
        LocalDate dateFinish = datePickerFinishDate.getValue();
        CarOwner carOwner = new CarOwner(idCar, idOwner, dateBegin, dateFinish);
        textFieldOwnerId.setText("");
        datePickerBeginDate.setValue(null);
        datePickerFinishDate.setValue(null);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
        Parent root;
        try {
            root = loader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = loader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            try {
                Dao<CarOwner> carOwnerDao = new CarOwnerDao(ConnectionControllerSingleton.getInstance());
                carOwnerDao.add(carOwner);
                statementViewController.setLabelStatementText("An attempt to insert data into functions succeed.");
            } catch (SQLException e) {
                e.printStackTrace();
                statementViewController.setLabelStatementText("An attempt to insert data into functions failed.");
            } finally {
                anchorPane.setDisable(true);
                stageResult.show();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void buttonBackOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    void setTextOfTextFieldCarId(String text) {
        this.textFieldCarId.setText(text);
    }
}
