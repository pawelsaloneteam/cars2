package pl.akademiakodu.homework.cars2.functions;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionControllerSingleton {
    private static final String hostName = "127.0.0.1";
    private static final int portNumber = 3306;
    private static final String dataBaseName = "cars2";
    private static final String userName = "root";
    private static final String userPassword = "qwer1234";
    private static Connection connection = null;
    private static ConnectionControllerSingleton instance = null;

    private ConnectionControllerSingleton() throws SQLException {
        MysqlDataSource mysqlDataSource = new MysqlDataSource();
        mysqlDataSource.setServerName(hostName);
        mysqlDataSource.setPort(portNumber);
        mysqlDataSource.setDatabaseName(dataBaseName);
        mysqlDataSource.setUser(userName);
        mysqlDataSource.setPassword(userPassword);
        mysqlDataSource.setServerTimezone("UTC");
        mysqlDataSource.setUseSSL(false);

        connection = mysqlDataSource.getConnection();
        connection.setAutoCommit(true);

        instance = this;
    }

    public static ConnectionControllerSingleton getInstance() throws SQLException {
        if (instance == null) {
            instance = new ConnectionControllerSingleton();
        }
        return instance;
    }

    public PreparedStatement prepareStatement(String preparedStatement) throws SQLException {
        return connection.prepareStatement(preparedStatement);
    }
}
