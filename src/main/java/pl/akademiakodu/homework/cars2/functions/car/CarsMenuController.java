package pl.akademiakodu.homework.cars2.functions.car;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.MainMenuController;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class CarsMenuController {

    private Stage stage;
    @FXML
    private AnchorPane anchorPane;


    public void showAddView() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/car_add.fxml"));
        Parent root = fxmlLoader.load();
        CarAddController carAddController = fxmlLoader.getController();
        carAddController.setStage(stage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void showAllCars() {
        Dao<Car> carDao;
        try {
            carDao = new CarDao(ConnectionControllerSingleton.getInstance());
            List<Car> cars = carDao.getAllItemsList();
            ShowAllCarsViewController showAllCarsViewController = ShowAllCarsViewController.showView(stage);
            showAllCarsViewController.setStage(stage);
            showAllCarsViewController.populateCarsList(cars);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                StatementViewController.showView("An attempt to retrieved cars list from DB failed.", anchorPane);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showMainMenu() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/main_menu.fxml"));
        Parent root = fxmlLoader.load();
        MainMenuController controllerMainMenu = fxmlLoader.getController();
        controllerMainMenu.setStage(stage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void showCar() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    static void showView(Stage stage) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(CarsMenuController.class.getClassLoader().getResource("ui/car/cars_menu.fxml"));
        Parent root;
        CarsMenuController carsMenuController;
        try {
            root = fxmlLoader.load();
            carsMenuController = fxmlLoader.getController();
            carsMenuController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
}
