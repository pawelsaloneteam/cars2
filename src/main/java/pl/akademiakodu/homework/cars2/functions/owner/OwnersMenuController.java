package pl.akademiakodu.homework.cars2.functions.owner;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.MainMenuController;

import java.io.IOException;
import java.sql.SQLException;

public class OwnersMenuController {

    private Stage stage;


    @FXML
    private void buttonAddOnActionListener() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/owners/owners_add.fxml"));
        Parent root = fxmlLoader.load();
        OwnersAddViewController ownersAddController = fxmlLoader.getController();
        ownersAddController.setStage(stage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void buttonListOwnersOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/owners/owners_list.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            OwnersListViewController ownersListViewController = fxmlLoader.getController();
            ownersListViewController.setStage(stage);
            try {
                ownersListViewController.populateList();
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void buttonBackOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/main_menu.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            MainMenuController controllerMainMenu = fxmlLoader.getController();
            controllerMainMenu.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
