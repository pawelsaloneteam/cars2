package pl.akademiakodu.homework.cars2.functions.inspection;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.MainMenuController;

import java.io.IOException;

public class InspectionsMenuController {

    private Stage stage;


    public void showInspectionAddView() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_add.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            InspectionAddController controllerInspectionAdd = fxmlLoader.getController();
            controllerInspectionAdd.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showInspectionsEditView() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_edit.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            InspectionEditController controllerInspectionEdit = fxmlLoader.getController();
            controllerInspectionEdit.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMainMenu() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/main_menu.fxml"));
        Parent root = fxmlLoader.load();
        MainMenuController controllerMainMenu = fxmlLoader.getController();
        controllerMainMenu.setStage(stage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
