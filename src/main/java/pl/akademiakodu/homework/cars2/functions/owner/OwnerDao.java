package pl.akademiakodu.homework.cars2.functions.owner;

import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class OwnerDao implements Dao<Owner> {
    private final ConnectionControllerSingleton connectionControllerSingleton;

    OwnerDao(ConnectionControllerSingleton connectionControllerSingleton) {
        this.connectionControllerSingleton = connectionControllerSingleton;
    }

    @Override
    public void add(Owner owner) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "INSERT INTO owner (surname, " +
                        "name" +
                        ") " +
                        "VALUES (?, " +
                        "?" +
                        ")"
        );

        preparedStatement.setString(1, owner.getSurname());
        preparedStatement.setString(2, owner.getName());
        preparedStatement.execute();
    }

    @Override
    public List<Owner> getAllItemsList() throws SQLException {
        List<Owner> result = new LinkedList<>();

        PreparedStatement preparedStatement = ConnectionControllerSingleton.getInstance().prepareStatement(
                "SELECT * " +
                        "FROM cars2.owner"
        );
        ResultSet resultSet = preparedStatement.executeQuery();


        while (resultSet.next()) {
            Owner owner = new Owner(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
            result.add(owner);
        }

        return result;
    }

    @Override
    public Optional<Owner> getIdSpecifiedItem(int id) {
        return Optional.empty();
        //todo implement
    }

    @Override
    public List<Owner> getItemsWhere(String column, String value) {
        return null;
    }

    @Override
    public void update(Owner oldCar, Owner newCar) {
        //todo implement
    }
}
