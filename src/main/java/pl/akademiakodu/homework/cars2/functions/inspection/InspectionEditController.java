package pl.akademiakodu.homework.cars2.functions.inspection;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Optional;

public class InspectionEditController {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldInspectionId;
    @FXML
    private TextField textFieldCarId;
    @FXML
    private TextField textFieldDate;
    @FXML
    private TextArea textAreaNote;

    private Stage stage;

    private Inspection inspectionOld;


    public void showInspecitonsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            InspectionsMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Inspection submenu view loading attempt failed.");
        }
    }

    public void updateDataBase() throws IOException {
        String textOfTextFieldCarId = textFieldCarId.getText();
        int carId;
        String dateOfInspectionString = textFieldDate.getText();
        LocalDate dateInspection;
        if (isInteger(textOfTextFieldCarId)) {
            if (isDateFormatValid(dateOfInspectionString)) {
                carId = Integer.parseInt(textOfTextFieldCarId);
                dateInspection = LocalDate.parse(dateOfInspectionString);
                Inspection inspectionNew = new Inspection(Integer.parseInt(textFieldInspectionId.getText()), carId, dateInspection, textAreaNote.getText());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                try {
                    root = fxmlLoader.load();
                    Scene scene = new Scene(root);
                    Stage stageResult = new Stage();
                    StatementViewController statementViewController = fxmlLoader.getController();
                    statementViewController.setStage(stageResult);
                    statementViewController.setAnchorPane(anchorPane);
                    stageResult.setScene(scene);
                    stageResult.setResizable(false);
                    stageResult.setAlwaysOnTop(true);
                    try {
                        Dao<Inspection> inspectionDao = new InspectionDao(ConnectionControllerSingleton.getInstance());
                        inspectionDao.update(inspectionOld, inspectionNew);
                        statementViewController.setLabelStatementText("An attempt of update data succeed.");

                    } catch (SQLException e) {
                        e.printStackTrace();
                        statementViewController.setLabelStatementText("An attempt to insert data failed.");

                    } finally {
                        anchorPane.setDisable(true);
                        stageResult.show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Wrong date. Enter proper date in format RRRR-MM-DD.");
                anchorPane.setDisable(true);
                stageResult.show();
            }
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Car ID is not a integer number.");
            anchorPane.setDisable(true);
            stageResult.show();
        }
    }

    private boolean isDateFormatValid(String date) {
        try {
            LocalDate.parse(date);
            return true;
        } catch (DateTimeException e) {
            return false;
        }

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }


    private boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    /*
        Reads entered inspection id number. Loads data from database. Sets loaded data to proper controls.
     */

    public void getInspection() throws IOException {
        String textTextFieldInspectionId = textFieldInspectionId.getText();
        if (isInteger(textTextFieldInspectionId)) {
            int inspectionId = Integer.parseInt(textTextFieldInspectionId);
            try {
                Dao<Inspection> inspectionDao = new InspectionDao(ConnectionControllerSingleton.getInstance());
                Optional<Inspection> optionalInspection = inspectionDao.getIdSpecifiedItem(inspectionId);
                optionalInspection.ifPresent(inspection -> this.inspectionOld = inspection);
            } catch (SQLException e) {
                e.printStackTrace();
            }


            String text = inspectionOld.getCarId() + "";
            textFieldCarId.setText(text);
            textFieldDate.setText(inspectionOld.getDate().toString());
            textAreaNote.setText(inspectionOld.getNote());


            textFieldInspectionId.setEditable(false);
            textFieldCarId.setEditable(true);
            textFieldDate.setEditable(true);
            textAreaNote.setEditable(true);


        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Inspection ID is not an integer number.");
            anchorPane.setDisable(true);
            stageResult.show();
        }
    }
}

