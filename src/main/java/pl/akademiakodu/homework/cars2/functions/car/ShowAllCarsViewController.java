package pl.akademiakodu.homework.cars2.functions.car;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class ShowAllCarsViewController {

    private Stage stage;
    @FXML
    private ListView<Label> listViewCars;

    public void handlerButtonBackOnAction() {
        CarsMenuController.showView(stage);
    }

    static ShowAllCarsViewController showView(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(ShowAllCarsViewController.class.getClassLoader().getResource("ui/car/show_all_cars_list_view.fxml"));
        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();

        return fxmlLoader.getController();
    }

    void setStage(Stage stage) {
        this.stage = stage;
    }

    void populateCarsList(List<Car> cars) {
        ObservableList<Label> items = FXCollections.observableArrayList();
        if (!cars.isEmpty()) {
            for (Car car : cars) {
                Label item = new Label();
                item.setText(
                        "ID: " + car.getId() + "\n" +
                                "Mark: " + car.getMark() + "\n" +
                                "Model: " + car.getModel() + "\n" +
                                "VIN: " + car.getVin()
                );
                items.add(item);
            }
        } else {
            Label item = new Label();
            item.setPrefWidth(listViewCars.getPrefWidth());
            item.setPrefHeight(100.0);
            item.setAlignment(Pos.CENTER);
            item.setText(
                    "No cars."
            );
            items.add(item);
        }

        listViewCars.setItems(items);
    }
}
