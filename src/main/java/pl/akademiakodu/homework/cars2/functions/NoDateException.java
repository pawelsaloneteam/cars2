package pl.akademiakodu.homework.cars2.functions;

public class NoDateException extends NullPointerException {
    public NoDateException(String errorMessage) {
        super(errorMessage);
    }
}
