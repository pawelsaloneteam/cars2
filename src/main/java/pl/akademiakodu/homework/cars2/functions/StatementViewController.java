package pl.akademiakodu.homework.cars2.functions;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;


public class StatementViewController {

    private Stage stage;
    private AnchorPane anchorPane;
    @FXML
    private Label labelStatement;


    /*
        Shows statement view. Returns controller to it.
     */


    public static void showView(String message, AnchorPane anchorPane) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(StatementViewController.class.getClassLoader().getResource("ui/statement_view.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);
        Stage stageResult = new Stage();
        StatementViewController statementViewController = fxmlLoader.getController();
        statementViewController.setStage(stageResult);
        statementViewController.setLabelStatementText(message);
        statementViewController.setAnchorPane(anchorPane);
        stageResult.setScene(scene);
        stageResult.setResizable(false);
        stageResult.setAlwaysOnTop(true);
        stageResult.show();

    }

    /*
        Makes previous view enable. Closes current window.
     */

    public void closeWindow() {
        anchorPane.setDisable(false);
        stage.close();

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setLabelStatementText(String text) {
        labelStatement.setText(text);
    }

    public void setAnchorPane(AnchorPane anchorPane) {
        this.anchorPane = anchorPane;
    }

    //todo make static function to show Statement View


}
