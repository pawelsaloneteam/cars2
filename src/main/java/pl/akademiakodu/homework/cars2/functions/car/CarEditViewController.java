package pl.akademiakodu.homework.cars2.functions.car;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;

public class CarEditViewController {

    private Car car;
    private Stage stage;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldId;
    @FXML
    private TextField textFieldMark;
    @FXML
    private TextField textFieldModel;
    @FXML
    private TextField textFieldVin;


    public void setCar(Car car) {
        this.car = car;
    }

/*
    Updates data in functions.
 */

    public void handlerButtonUpdateOnAction() {
        Car newCar = new Car(car.getId(), textFieldMark.getText(), textFieldModel.getText(), textFieldVin.getText());
        Dao<Car> carDao;
        try {
            carDao = new CarDao(ConnectionControllerSingleton.getInstance());
            carDao.update(car, newCar);
            anchorPane.setDisable(true);
            StatementViewController.showView("An attempt to update car date succeeded.", anchorPane);
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                StatementViewController.showView("An attempt to update car date failed.", anchorPane);
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /*
        Shows Show Car View.
     */

    public void handlerButtonBackOnAction() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public TextField getTextFieldId() {
        return textFieldId;
    }

    TextField getTextFieldMark() {
        return textFieldMark;
    }

    TextField getTextFieldModel() {
        return textFieldModel;
    }

    TextField getTextFieldVin() {
        return textFieldVin;
    }
}
