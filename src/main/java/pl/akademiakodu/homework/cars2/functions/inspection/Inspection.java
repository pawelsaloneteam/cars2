package pl.akademiakodu.homework.cars2.functions.inspection;

import java.time.LocalDate;

public class Inspection {
    private int id;
    private int car_id;
    private LocalDate date;
    private String note;

    public Inspection(int car_id, LocalDate date, String note) {
        this.id = 0;
        this.car_id = car_id;
        this.date = date;
        this.note = note;
    }

    public Inspection(int id, int car_id, LocalDate date, String note) {
        this.id = id;
        this.car_id = car_id;
        this.date = date;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    int getCarId() {
        return car_id;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getNote() {
        return note;
    }
}
