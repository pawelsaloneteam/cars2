package pl.akademiakodu.homework.cars2.functions.collision;

import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.NoDateException;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CollisionDao implements Dao<pl.akademiakodu.homework.cars2.functions.collision.Collision> {
    private ConnectionControllerSingleton connectionControllerSingleton;

    public CollisionDao(ConnectionControllerSingleton connectionControllerSingleton) {
        this.connectionControllerSingleton = connectionControllerSingleton;
    }

    @Override
    public void add(pl.akademiakodu.homework.cars2.functions.collision.Collision collision) throws SQLException, NoDateException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "INSERT INTO collision (car_id, date, place, note) " +
                        "VALUES (?, ?, ?, ?)"
        );
        preparedStatement.setInt(1, collision.getCarId());
        if (collision.getDate() == null) {
            throw new NoDateException("No date provided or wrong date format.");
        } else {
            preparedStatement.setString(2, collision.getDate().toString());
        }
        preparedStatement.setString(3, collision.getPlace());
        preparedStatement.setString(4, collision.getNote());
        preparedStatement.execute();
    }

    @Override
    public List<pl.akademiakodu.homework.cars2.functions.collision.Collision> getAllItemsList() throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT *" +
                        "FROM collision"
        );
        ResultSet resultSet = preparedStatement.executeQuery();

        List<pl.akademiakodu.homework.cars2.functions.collision.Collision> collisions = new LinkedList<>();

        while (resultSet.next()) {
            pl.akademiakodu.homework.cars2.functions.collision.Collision collision = new pl.akademiakodu.homework.cars2.functions.collision.Collision(resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getDate(3).toLocalDate(),
                    resultSet.getString(4),
                    resultSet.getString(5)
            );
            collisions.add(collision);
        }

        return collisions;
    }

    @Override
    public Optional<pl.akademiakodu.homework.cars2.functions.collision.Collision> getIdSpecifiedItem(int id) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM collision " +
                        "WHERE id=?"
        );
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        Optional<pl.akademiakodu.homework.cars2.functions.collision.Collision> optionalCollision;
        if (resultSet.next()) {
            pl.akademiakodu.homework.cars2.functions.collision.Collision collision = new pl.akademiakodu.homework.cars2.functions.collision.Collision(resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getDate(3).toLocalDate(),
                    resultSet.getString(4),
                    resultSet.getString(5)
            );
            optionalCollision = Optional.of(collision);
        } else {
            optionalCollision = Optional.empty();
        }

        return optionalCollision;
    }

    @Override
    public void update(pl.akademiakodu.homework.cars2.functions.collision.Collision oldCollision, pl.akademiakodu.homework.cars2.functions.collision.Collision newCollision) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "UPDATE collision " +
                        "SET car_id = ?, date = ?, place = ?, note = ? " +
                        "WHERE id = ?"
        );
        preparedStatement.setInt(1, newCollision.getCarId());
        preparedStatement.setString(2, newCollision.getDate().toString());
        preparedStatement.setString(3, newCollision.getPlace());
        preparedStatement.setString(4, newCollision.getNote());
        preparedStatement.setInt(5, oldCollision.getId());
        preparedStatement.execute();
    }

    /*
        Retreive items from database with condition that value in column column is value.
     */

    @Override
    public List<pl.akademiakodu.homework.cars2.functions.collision.Collision> getItemsWhere(String column, String value) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM cars2.collision " +
                        "WHERE " + column + " = ?"
        );
        preparedStatement.setInt(1, Integer.parseInt(value));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Collision> collisions = new LinkedList<>();

        while (resultSet.next()) {
            Collision collision = new Collision(
                    resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getDate(3).toLocalDate(),
                    resultSet.getString(4),
                    resultSet.getString(5));


            collisions.add(collision);
        }


        return collisions;
    }
}
