package pl.akademiakodu.homework.cars2.functions.car;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;

public class CarAddController {
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldModel;
    @FXML
    private TextField textFieldMark;
    @FXML
    private TextField textFieldVin;

    private Stage stage;

    public void showCarsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/cars_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            CarsMenuController carsMenuController = fxmlLoader.getController();
            carsMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cars submenu view loading attempt failed.");
        }
    }

    /*
        Gets connection to functions. Make INSERT INTO statement. Displays window with result statement.
     */
    public void addToDataBase() {
        Car car = new Car(textFieldMark.getText(), textFieldModel.getText(), textFieldVin.getText());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            try {
                Dao<Car> carDao = new CarDao(ConnectionControllerSingleton.getInstance());
                carDao.add(car);
                statementViewController.setLabelStatementText("An attempt to insert data into functions succeed.");

            } catch (SQLException e) {
                e.printStackTrace();
                statementViewController.setLabelStatementText("An attempt to insert data into functions failed.");

            } finally {
                anchorPane.setDisable(true);
                stageResult.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
