package pl.akademiakodu.homework.cars2.functions.collision;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.car.ShowCarController;

import java.io.IOException;
import java.util.List;

public class CollisionsShowViewController {

    private Stage stage;
    @FXML
    private TextField textFieldId;
    @FXML
    private ListView<Label> listViewCollisions;


    public void populateCollisionsList(List<Collision> collisions) {
        ObservableList<Label> items = FXCollections.observableArrayList();
        if (!collisions.isEmpty()) {
            for (Collision collision : collisions) {
                Label item = new Label();
//                item.setPrefWidth(listViewCollisions.getPrefWidth());
//                item.setPrefHeight(100.0);
                item.setText(
                        "ID: " + collision.getId() + "\n" +
                                "Car ID: " + collision.getCarId() + "\n" +
                                "Date: " + collision.getDate() + "\n" +
                                "Place: " + collision.getPlace() + "\n" +
                                "Note: " + collision.getNote()
                );
                items.add(item);
            }
        } else {
            Label item = new Label();
            item.setPrefWidth(listViewCollisions.getPrefWidth());
            item.setPrefHeight(100.0);
            item.setAlignment(Pos.CENTER);
            item.setText(
                    "No collisions."
            );
            items.add(item);
        }

        listViewCollisions.setItems(items);

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }


    public void showCar() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void setTextFieldIdText(String id) {
        textFieldId.setText(id);
    }
}
