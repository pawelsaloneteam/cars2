package pl.akademiakodu.homework.cars2.functions.car;

import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CarDao implements Dao<Car> {
    private ConnectionControllerSingleton connectionControllerSingleton;


    CarDao(ConnectionControllerSingleton connectionControllerSingleton) {
        this.connectionControllerSingleton = connectionControllerSingleton;
    }


    @Override
    public void add(Car car) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "INSERT INTO car (mark, " +
                        "model, " +
                        "vin" +
                        ") " +
                        "VALUES (?, " +
                        "?, " +
                        "?" +
                        ")"
        );

        preparedStatement.setString(1, car.getMark());
        preparedStatement.setString(2, car.getModel());
        preparedStatement.setString(3, car.getVin());
        preparedStatement.execute();
    }

    /*
        Returns List of Car object created of data retrieved from table car of functions.  Queries all data from table car.
        1. Download all data.
        2. Put received data to a Collection.
        3. Return.
     */

    @Override
    public List<Car> getAllItemsList() throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT *" +
                        "FROM car"
        );
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Car> cars = new LinkedList<>();

        while (resultSet.next()) {
            Car car = new Car(resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4)
            );
            cars.add(car);
        }

        return cars;
    }

    /*
        Returns car object retrieved from car table in functions. Match id parameter to id column value in the table.
        Downloads from db only one car data.
        1. Get data from functions.
        2. Create car object using retrieved data.
        3. Return created object.
     */

    @Override
    public Optional<Car> getIdSpecifiedItem(int id) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM car " +
                        "WHERE id=?"
        );
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        Optional<Car> carOptional;
        if (resultSet.next()) {
            Car car = new Car(resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4)
            );
            carOptional = Optional.of(car);
        } else {
            carOptional = Optional.empty();
        }

        return carOptional;
    }

    /*
        Updates data in functions. Identifies Car to update by id. Changes values of columns mark, model, vin.
     */

    @Override
    public void update(Car oldCar, Car newCar) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "UPDATE car " +
                        "SET mark = ?, model = ?, vin = ? " +
                        "WHERE id = ?"
        );
        preparedStatement.setString(1, newCar.getMark());
        preparedStatement.setString(2, newCar.getModel());
        preparedStatement.setString(3, newCar.getVin());
        preparedStatement.setInt(4, oldCar.getId());
        preparedStatement.execute();
    }


    @Override
    public List<Car> getItemsWhere(String column, String value) {
        return null;
    }
}
