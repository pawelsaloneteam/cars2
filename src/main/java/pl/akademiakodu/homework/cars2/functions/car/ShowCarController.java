package pl.akademiakodu.homework.cars2.functions.car;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.car.car_owner.*;
import pl.akademiakodu.homework.cars2.functions.collision.Collision;
import pl.akademiakodu.homework.cars2.functions.collision.CollisionDao;
import pl.akademiakodu.homework.cars2.functions.inspection.Inspection;
import pl.akademiakodu.homework.cars2.functions.inspection.InspectionDao;
import pl.akademiakodu.homework.cars2.functions.collision.CollisionsShowViewController;
import pl.akademiakodu.homework.cars2.functions.inspection.InspectionsShowViewController;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ShowCarController {

    @FXML
    private Button buttonListOwners;
    @FXML
    private Button buttonSetOwner;
    @FXML
    private Button buttonOwnerPresent;
    @FXML
    private TextField textFieldId;
    @FXML
    private TextField textFieldModel;
    @FXML
    private TextField textFieldVin;
    @FXML
    private Button buttonEdit;
    @FXML
    private TextField textFieldMark;
    @FXML
    private Button buttonCollisions;
    @FXML
    private Button buttonInspections;
    @FXML
    private AnchorPane anchorPane;

    private Stage stage;

    private Car retrievedCar;

    /*
        Retrieve data from functions using inform specified car id number.
        Puts retrieved data to proper textFields objects.
     */


    public void handlerButtonLoadOnAction() {

        Car carToGet = new Car(Integer.parseInt(textFieldId.getText()));
        try {
            Dao<Car> carDao = new CarDao(ConnectionControllerSingleton.getInstance());

            Optional<Car> carOptional = carDao.getIdSpecifiedItem(carToGet.getId());
            if (carOptional.isPresent()) {
                retrievedCar = carOptional.get();
                textFieldMark.setText(retrievedCar.getMark());
                textFieldModel.setText(retrievedCar.getModel());
                textFieldVin.setText(retrievedCar.getVin());
                buttonEdit.setDisable(false);
                buttonCollisions.setDisable(false);
                buttonInspections.setDisable(false);
                buttonSetOwner.setDisable(false);
                buttonOwnerPresent.setDisable(false);
                buttonListOwners.setDisable(false);
            } else {
                showFailStatement();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            showFailStatement();
        }
    }

    /*
        Show windows with information about failure of retrieving data from functions.
     */

    private void showFailStatement() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResultStatement = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResultStatement);
            statementViewController.setAnchorPane(anchorPane);
            statementViewController.setLabelStatementText("Car retrieving from functions failed.");
            stageResultStatement.setScene(scene);
            stageResultStatement.setResizable(false);
            stageResultStatement.setAlwaysOnTop(true);
            anchorPane.setDisable(true);
            stageResultStatement.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    Shows Edit car view. Sets proper values in textFields of edit car view.
     */

    public void showEditCarView() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/car_edit_view.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            CarEditViewController carEditViewController = fxmlLoader.getController();
            carEditViewController.setCar(retrievedCar);
            carEditViewController.setStage(stage);
            TextField textFieldIdOfCarEditView = carEditViewController.getTextFieldId();
            textFieldIdOfCarEditView.setText("" + retrievedCar.getId());
            textFieldIdOfCarEditView.setDisable(true);
            carEditViewController.getTextFieldMark().setText(retrievedCar.getMark());
            carEditViewController.getTextFieldModel().setText(retrievedCar.getModel());
            carEditViewController.getTextFieldVin().setText(retrievedCar.getVin());
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
        Retrieved all collisions from db. Shows view. Populate view by Collision objects.
     */

    public void showCollisions() {
        Dao<Collision> collisionDao;
        List<Collision> collisions;
        try {
            collisionDao = new CollisionDao(ConnectionControllerSingleton.getInstance());
            collisions = collisionDao.getItemsWhere("car_id", textFieldId.getText());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_show_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                CollisionsShowViewController collisionsShowViewController = fxmlLoader.getController();
                collisionsShowViewController.setTextFieldIdText("" + retrievedCar.getId());
                collisionsShowViewController.populateCollisionsList(collisions);
                collisionsShowViewController.setStage(stage);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            anchorPane.setDisable(true);
            try {
                StatementViewController.showView("An attempt to retrieve data from db failed", anchorPane);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    /*
        Retrieve inspection data from db. Shows inspection.
     */

    public void showInspections() {
        Dao<Inspection> inspectionDao;
        List<Inspection> inspections;
        try {
            inspectionDao = new InspectionDao(ConnectionControllerSingleton.getInstance());
            inspections = inspectionDao.getItemsWhere("car_id", textFieldId.getText());
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_show_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                InspectionsShowViewController inspectionsShowViewController = fxmlLoader.getController();
                inspectionsShowViewController.setTextFieldIdText("" + retrievedCar.getId());
                inspectionsShowViewController.populateInspectionsList(inspections);
                inspectionsShowViewController.setStage(stage);
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            anchorPane.setDisable(true);
            try {
                StatementViewController.showView("An attempt to retrieve data from db failed", anchorPane);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void showCarsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/cars_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            CarsMenuController carsMenuController = fxmlLoader.getController();
            carsMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cars submenu view loading attempt failed.");
        }
    }

    public void handlerButtonSetOwnerOnAction() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/car_set_owner.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            CarSetOwnerViewController carSetOwnerViewController = fxmlLoader.getController();
            carSetOwnerViewController.setStage(stage);
            carSetOwnerViewController.setTextOfTextFieldCarId(textFieldId.getText());

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void handlerButtonOwnerPresentOnAction() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/car_owner_present.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();

            try {

                Dao<CarOwner> carOwnerDao = new CarOwnerDao(ConnectionControllerSingleton.getInstance());
                List<CarOwner> carOwners = carOwnerDao.getItemsWhere("car_id", textFieldId.getText());

                CarPresentOwnerController carPresentOwnerController = fxmlLoader.getController();
                carPresentOwnerController.setStage(stage);
                carPresentOwnerController.setTextOfTextFieldCarId(textFieldId.getText());
                carPresentOwnerController.populateCarOwnerList(carOwners);


                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void buttonListOwnersOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/car_owner_list.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();

            try {

                Dao<CarOwner> carOwnerDao = new CarOwnerDao(ConnectionControllerSingleton.getInstance());
                List<CarOwner> carOwners = carOwnerDao.getItemsWhere("car_id", textFieldId.getText());

                CarOwnerListController carOwnerListController = fxmlLoader.getController();
                carOwnerListController.setStage(stage);
                carOwnerListController.setTextOfTextFieldCarId(textFieldId.getText());
                carOwnerListController.populateCarOwnerList(carOwners);


                Scene scene = new Scene(root);
                stage.setScene(scene);
                stage.show();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();

        }

    }
}
