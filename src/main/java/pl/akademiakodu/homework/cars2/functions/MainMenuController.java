package pl.akademiakodu.homework.cars2.functions;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.car.CarsMenuController;
import pl.akademiakodu.homework.cars2.functions.collision.CollisionsMenuController;
import pl.akademiakodu.homework.cars2.functions.inspection.InspectionsMenuController;
import pl.akademiakodu.homework.cars2.functions.owner.OwnersMenuController;

import java.io.IOException;

public class MainMenuController {
    private Stage stage;



    public void showCarsMenu() {

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/cars_menu.fxml"));  //works todo clean comments
        
        try {
            Parent root = fxmlLoader.load();
            CarsMenuController carsMenuController = fxmlLoader.getController();
            carsMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cars submenu view loading attempt failed.");
        }

    }

    public void showInspectionsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_menu.fxml"));  //works todo clean comments

        try {
            Parent root = fxmlLoader.load();
            InspectionsMenuController inspectionsMenuController = fxmlLoader.getController();
            inspectionsMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("inspection submenu view loading attempt failed.");
        }

    }

    public void showCollisionsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_menu.fxml"));  //works todo clean comments
        
        try {
            Parent root = fxmlLoader.load();
            CollisionsMenuController collisionsMenuController = fxmlLoader.getController();
            collisionsMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Collisions submenu view loading attempt failed.");
        }

    }

    public void showOwnersMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/owners/owners_menu.fxml"));  //works todo clean comments
        
        try {
            Parent root = fxmlLoader.load();
            OwnersMenuController ownersMenuController = fxmlLoader.getController();
            ownersMenuController.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Owners submenu view loading attempt failed.");
        }

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
