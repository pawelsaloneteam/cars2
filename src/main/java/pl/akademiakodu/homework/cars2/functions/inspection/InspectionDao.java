package pl.akademiakodu.homework.cars2.functions.inspection;

import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class InspectionDao implements Dao<Inspection> {
    private final ConnectionControllerSingleton connectionControllerSingleton;

    public InspectionDao(ConnectionControllerSingleton connectionControllerSingleton) {
        this.connectionControllerSingleton = connectionControllerSingleton;
    }

    @Override
    public void add(Inspection inspection) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "INSERT INTO inspection (car_id, " +
                        "date, " +
                        "note" +
                        ") " +
                        "VALUES (?, " +
                        "?, " +
                        "?" +
                        ")"
        );

        preparedStatement.setInt(1, inspection.getCarId());
        preparedStatement.setString(2, inspection.getDate().toString());
        preparedStatement.setString(3, inspection.getNote());
        preparedStatement.execute();
    }

    @Override
    public List<Inspection> getAllItemsList() throws SQLException {
        return null;
    }

    @Override
    public Optional<Inspection> getIdSpecifiedItem(int id) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM cars2.inspection " +
                        "WHERE id=?"
        );

        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            Inspection inspection = new Inspection(resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getDate(3).toLocalDate(),
                    resultSet.getString(4)
            );
            return Optional.of(inspection);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void update(Inspection oldInspection, Inspection newInspection) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "UPDATE cars2.inspection " +
                        "SET car_id = ?,  date = ?,  note = ? " +
                        "WHERE id = ?"
        );
        preparedStatement.setInt(1, newInspection.getCarId());
        preparedStatement.setString(2, newInspection.getDate().toString());
        preparedStatement.setString(3, newInspection.getNote());
        preparedStatement.setInt(4, oldInspection.getId());
        preparedStatement.execute();
    }

    @Override
    public List<Inspection> getItemsWhere(String column, String value) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM cars2.inspection " +
                        "WHERE " + column + " = ?"
        );
        preparedStatement.setInt(1, Integer.parseInt(value));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Inspection> inspections = new LinkedList<>();

        while (resultSet.next()) {
            Inspection inspection = new Inspection(
                    resultSet.getInt(1),
                    resultSet.getInt(2),
                    resultSet.getDate(3).toLocalDate(),
                    resultSet.getString(4));

            inspections.add(inspection);
        }
        return inspections;
    }
}
