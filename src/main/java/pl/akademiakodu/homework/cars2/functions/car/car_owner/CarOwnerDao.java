package pl.akademiakodu.homework.cars2.functions.car.car_owner;

import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CarOwnerDao implements Dao<CarOwner> {
    private ConnectionControllerSingleton connectionControllerSingleton;


    public CarOwnerDao(ConnectionControllerSingleton connectionControllerSingleton) {
        this.connectionControllerSingleton = connectionControllerSingleton;
    }

    @Override
    public void add(CarOwner item) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement("" +
                "INSERT INTO cars2.car_owner(car_id, owner_id, buy_date, sell_date)" +
                "values (?, ?, ?, ?)"
        );
        preparedStatement.setInt(1, item.getIdCar());
        preparedStatement.setInt(2, item.getIdOwner());
        preparedStatement.setString(3, item.getDateBegin().toString());
        preparedStatement.setString(4, item.getDateFinish().toString());
        preparedStatement.execute();
    }

    @Override
    public List<CarOwner> getAllItemsList() {
        return null;
    }

    @Override
    public Optional<CarOwner> getIdSpecifiedItem(int id) {
        return Optional.empty();
    }

    @Override
    public void update(CarOwner oldItem, CarOwner newItem) {

    }

    @Override
    public List<CarOwner> getItemsWhere(String column, String value) throws SQLException {
        PreparedStatement preparedStatement = connectionControllerSingleton.prepareStatement(
                "SELECT * " +
                        "FROM cars2.car_owner " +
                        "WHERE " + column + " = ?"
        );
        preparedStatement.setInt(1, Integer.parseInt(value));
        ResultSet resultSet = preparedStatement.executeQuery();
        List<CarOwner> carOwners = new LinkedList<>();

        while (resultSet.next()) {
            CarOwner carOwner = new CarOwner(
                    resultSet.getInt(2),
                    resultSet.getInt(3),
                    resultSet.getDate(4).toLocalDate(),
                    resultSet.getDate(5).toLocalDate()
            );


            carOwners.add(carOwner);
        }

        return carOwners;
    }
}
