package pl.akademiakodu.homework.cars2.functions.collision;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.MainMenuController;

import java.io.IOException;

public class CollisionsMenuController {

    private Stage stage;


    public void showCollisionAddView() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_add.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            CollisionAddController controllerCollisionAdd = fxmlLoader.getController();
            controllerCollisionAdd.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showCollisionEditView() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_edit.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            CollisionEditController controllerCollisionEdit = fxmlLoader.getController();
            controllerCollisionEdit.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMainMenu() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/main_menu.fxml"));
        Parent root = fxmlLoader.load();
        MainMenuController controllerMainMenu = fxmlLoader.getController();
        controllerMainMenu.setStage(stage);

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
