package pl.akademiakodu.homework.cars2.functions.inspection;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;

public class InspectionAddController {

    private Stage stage;
    @FXML
    private TextField textFieldCarId;
    @FXML
    private TextField textFieldDate;
    @FXML
    private TextArea textAreaNote;
    @FXML
    private AnchorPane anchorPane;


    public void showInspectionsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/inspection/inspection_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            InspectionsMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Inspection submenu view loading attempt failed.");
        }
    }

    /*
        Make INSERT INTO statement. Displays window with result statement.
     */
    public void addToDataBase() throws IOException {
        String textOfTextFieldCarId = textFieldCarId.getText();
        int carId;
        String dateOfInspectionString = textFieldDate.getText();
        LocalDate dateInspection;
        if (isInteger(textOfTextFieldCarId)) {
            if (isDateFormatValid(dateOfInspectionString)) {
                carId = Integer.parseInt(textOfTextFieldCarId);
                dateInspection = LocalDate.parse(dateOfInspectionString);
                Inspection inspection = new Inspection(0, carId, dateInspection, textAreaNote.getText());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                try {
                    root = fxmlLoader.load();
                    Scene scene = new Scene(root);
                    Stage stageResult = new Stage();
                    StatementViewController statementViewController = fxmlLoader.getController();
                    statementViewController.setStage(stageResult);
                    statementViewController.setAnchorPane(anchorPane);
                    stageResult.setScene(scene);
                    stageResult.setResizable(false);
                    stageResult.setAlwaysOnTop(true);
                    try {
                        Dao<Inspection> inspectionDao = new InspectionDao(ConnectionControllerSingleton.getInstance());
                        inspectionDao.add(inspection);
                        statementViewController.setLabelStatementText("An attempt to insert data into functions succeed.");

                    } catch (SQLException e) {
                        e.printStackTrace();
                        statementViewController.setLabelStatementText("An attempt to insert data into functions failed.");

                    } finally {
                        anchorPane.setDisable(true);
                        stageResult.show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Wrong date. Enter proper date in format RRRR-MM-DD.");
                anchorPane.setDisable(true);
                stageResult.show();
            }
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Car ID is not a integer number.");
            anchorPane.setDisable(true);
            stageResult.show();
        }
    }

    private boolean isDateFormatValid(String date) {
        try {
            LocalDate.parse(date);
            return true;
        } catch (DateTimeException e) {
            return false;
        }

    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }


    private boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }
}
