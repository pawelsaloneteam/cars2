package pl.akademiakodu.homework.cars2.functions.owner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class OwnersListViewController {

    private Stage stage;
    @FXML
    private ListView<Label> listViewOwners;


    void populateList() throws SQLException {

        Dao<Owner> ownerDao = new OwnerDao(ConnectionControllerSingleton.getInstance());
        List<Owner> owners = ownerDao.getAllItemsList();


        ObservableList<Label> observableList = FXCollections.observableArrayList();
        if (!owners.isEmpty()) {
            for (Owner owner : owners) {
                Label item = new Label();
                item.setText(owner.toString());
                observableList.add(item);
            }
        }

        listViewOwners.setItems(observableList);
    }

    @FXML
    private void buttonBackOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/owners/owners_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            OwnersMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Owners submenu view loading attempt failed.");
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
