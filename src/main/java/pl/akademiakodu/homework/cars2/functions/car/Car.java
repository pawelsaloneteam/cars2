package pl.akademiakodu.homework.cars2.functions.car;

public class Car {
    private int id;
    private String mark;
    private String model;
    private String vin;

    public Car(int id) {
        this.id = id;
        mark = "";
        model = "";
        vin = "";
    }

    public Car(String mark, String model, String vin) {
        this.id = 0;
        this.mark = mark;
        this.model = model;
        this.vin = vin;
    }

    public Car(int id, String mark, String model, String vin) {
        this.id = id;
        this.mark = mark;
        this.model = model;
        this.vin = vin;
    }

    public int getId() {
        return id;
    }

    String getMark() {
        return mark;
    }

    public String getModel() {
        return model;
    }

    String getVin() {
        return vin;
    }
}
