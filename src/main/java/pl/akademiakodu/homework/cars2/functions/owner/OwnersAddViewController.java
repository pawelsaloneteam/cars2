package pl.akademiakodu.homework.cars2.functions.owner;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;

public class OwnersAddViewController {

    private Stage stage;
    @FXML
    private TextField textFieldSurname;
    @FXML
    private TextField textFieldName;
    @FXML
    private AnchorPane anchorPane;


    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void buttonAddOnActionListener() {
        Owner owner = new Owner(textFieldSurname.getText(), textFieldName.getText());
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            try {
                Dao<Owner> ownerDao = new OwnerDao(ConnectionControllerSingleton.getInstance());
                ownerDao.add(owner);
                statementViewController.setLabelStatementText("An attempt to insert data into functions succeed.");
                textFieldName.setText("");
                textFieldSurname.setText("");
            } catch (SQLException e) {
                e.printStackTrace();
                statementViewController.setLabelStatementText("An attempt to insert data into functions failed.");

            } finally {
                anchorPane.setDisable(true);
                stageResult.show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void buttonBackOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/owners/owners_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            OwnersMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Owners submenu view loading attempt failed.");
        }
    }
}
