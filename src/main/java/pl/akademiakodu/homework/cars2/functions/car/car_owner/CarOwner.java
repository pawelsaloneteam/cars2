package pl.akademiakodu.homework.cars2.functions.car.car_owner;

import java.time.LocalDate;

public class CarOwner {
    private int id;
    private int idCar;
    private int idOwner;
    private LocalDate dateBegin;
    private LocalDate dateFinish;


    public CarOwner(int idCar, int idOwner, LocalDate dateBegin, LocalDate dateFinish) {
        this.id = 0;
        this.idCar = idCar;
        this.idOwner = idOwner;
        this.dateBegin = dateBegin;
        this.dateFinish = dateFinish;
    }

    public int getId() {
        return id;
    }

    int getIdCar() {
        return idCar;
    }

    int getIdOwner() {
        return idOwner;
    }

    LocalDate getDateBegin() {
        return dateBegin;
    }

    LocalDate getDateFinish() {
        return dateFinish;
    }
}
