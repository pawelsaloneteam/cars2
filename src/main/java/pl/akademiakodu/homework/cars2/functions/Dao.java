package pl.akademiakodu.homework.cars2.functions;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Dao<T> {

    void add(T item) throws SQLException;

    List<T> getAllItemsList() throws SQLException;

    Optional<T> getIdSpecifiedItem(int id) throws SQLException;

    void update(T oldItem, T newItem) throws SQLException;

    List<T> getItemsWhere(String column, String value) throws SQLException;

}
