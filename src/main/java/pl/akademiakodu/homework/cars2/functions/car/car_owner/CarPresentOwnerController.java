package pl.akademiakodu.homework.cars2.functions.car.car_owner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.car.ShowCarController;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class CarPresentOwnerController {

    private Stage stage;
    @FXML
    private ListView<Label> listViewCars;
    @FXML
    private TextField textFieldCarId;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private void handlerButtonBackOnAction() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void populateCarOwnerList(List<CarOwner> carOwners) {
        ObservableList<Label> items = FXCollections.observableArrayList();


        if (!carOwners.isEmpty()) {
            for (CarOwner carOwner : carOwners) {
                if (carOwner.getDateFinish().isAfter(LocalDate.now())) {
                    Label item = new Label();
                    item.setText(
                            "Car Id: " + carOwner.getIdCar() + "\n" +
                                    "Owner Id: " + carOwner.getIdOwner() + "\n" +
                                    "Buy date: " + carOwner.getDateBegin()
                    );
                    items.add(item);
                }
            }
        } else {
            Label item = new Label();
            item.setPrefWidth(listViewCars.getPrefWidth());
            item.setPrefHeight(100.0);
            item.setAlignment(Pos.CENTER);
            item.setText(
                    "No present owners."
            );
            items.add(item);
        }

        listViewCars.setItems(items);
    }

    public void setTextOfTextFieldCarId(String text) {
        this.textFieldCarId.setText(text);
    }
}
