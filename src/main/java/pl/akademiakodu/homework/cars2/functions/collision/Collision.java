package pl.akademiakodu.homework.cars2.functions.collision;

/*
    Dat model class.
 */

import java.time.LocalDate;

public class Collision {
    private int id;
    private int carId;
    private LocalDate date;
    private String place;
    private String note;

    public Collision(int id, int carId, LocalDate date, String place, String note) {
        this.id = id;
        this.carId = carId;
        this.date = date;
        this.place = place;
        this.note = note;
    }

    public Collision(int carId, LocalDate date, String place, String note) {
        this.carId = carId;
        this.date = date;
        this.place = place;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    int getCarId() {
        return carId;
    }

    public LocalDate getDate() {
        return date;
    }

    String getPlace() {
        return place;
    }

    public String getNote() {
        return note;
    }
}
