package pl.akademiakodu.homework.cars2.functions.collision;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

public class CollisionEditController {
    @FXML
    private TextField textFieldPlace;
    @FXML
    private DatePicker datePickerDate;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TextField textFieldCollisionId;
    @FXML
    private TextField textFieldCarId;
    @FXML
    private TextArea textAreaNote;

    private Stage stage;

    private Collision collisionOld;

    /*
        Shows Collision menu.
     */

    public void buttonBackOnActionListener() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            CollisionsMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Collision submenu view loading attempt failed.");
        }
    }

    /*
        Listener on action event of button Update. Changes collision data in database.
     */

    public void buttonUpdateOnActionListener() throws IOException {
        String textOfTextFieldCarId = textFieldCarId.getText();
        int carId;
        LocalDate dateCollision;
        if (isInteger(textOfTextFieldCarId)) {
            if (datePickerDate.getValue() != null) {
                carId = Integer.parseInt(textOfTextFieldCarId);
                dateCollision = datePickerDate.getValue();
                Collision collisionNew = new Collision(Integer.parseInt(textFieldCollisionId.getText()), carId, dateCollision, textFieldPlace.getText(), textAreaNote.getText());
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                try {
                    root = fxmlLoader.load();
                    Scene scene = new Scene(root);
                    Stage stageResult = new Stage();
                    StatementViewController statementViewController = fxmlLoader.getController();
                    statementViewController.setStage(stageResult);
                    statementViewController.setAnchorPane(anchorPane);
                    stageResult.setScene(scene);
                    stageResult.setResizable(false);
                    stageResult.setAlwaysOnTop(true);
                    try {
                        Dao<Collision> collisionDao = new CollisionDao(ConnectionControllerSingleton.getInstance());
                        collisionDao.update(collisionOld, collisionNew);
                        statementViewController.setLabelStatementText("An attempt of update data succeed.");
                        textFieldCollisionId.setEditable(true);
                        textFieldCarId.setEditable(false);
                        datePickerDate.setEditable(false);
                        textFieldPlace.setEditable(false);
                        textAreaNote.setEditable(false);

                    } catch (SQLException e) {
                        e.printStackTrace();
                        statementViewController.setLabelStatementText("An attempt to insert data failed.");

                    } finally {
                        anchorPane.setDisable(true);
                        stageResult.show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
                Parent root;
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                statementViewController.setLabelStatementText("Wrong date. Enter proper date in format dd.mm.yyyy.");
                anchorPane.setDisable(true);
                stageResult.show();
            }
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Car ID is not a integer number.");
            anchorPane.setDisable(true);
            stageResult.show();
        }
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /*
        Check if String number can be parsed to Integer.
     */

    private boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    /*
        Reads entered collision id number. Loads data from database. Sets loaded data to proper controls.
     */

    public void buttonLoadOnActionListener() throws IOException {
        String textTextFieldCollisionId = textFieldCollisionId.getText();
        if (isInteger(textTextFieldCollisionId)) {
            int collisionId = Integer.parseInt(textTextFieldCollisionId);
            try {
                Dao<Collision> collisionDao = new CollisionDao(ConnectionControllerSingleton.getInstance());
                Optional<Collision> optionalCollision = collisionDao.getIdSpecifiedItem(collisionId);
                optionalCollision.ifPresent(collision -> this.collisionOld = collision);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            String text = collisionOld.getCarId() + "";
            textFieldCarId.setText(text);
            datePickerDate.setValue(collisionOld.getDate());
            textFieldPlace.setText(collisionOld.getPlace());
            textAreaNote.setText(collisionOld.getNote());
            textFieldCollisionId.setEditable(false);
            textFieldCarId.setEditable(true);
            datePickerDate.setEditable(true);
            textFieldPlace.setEditable(true);
            textAreaNote.setEditable(true);

        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Collision ID is not an integer number.");
            anchorPane.setDisable(true);
            stageResult.show();

        }
    }
}
