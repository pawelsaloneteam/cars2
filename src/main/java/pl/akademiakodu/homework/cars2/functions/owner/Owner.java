package pl.akademiakodu.homework.cars2.functions.owner;

public class Owner {
    private int id;
    private String surname;
    private String name;

    public Owner(int id, String surname, String name) {
        this.id = id;
        this.surname = surname;
        this.name = name;
    }

    public Owner(String surname, String name) {
        this.id = 0;
        this.surname = surname;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + ". " + surname + ", " + name;
    }
}
