package pl.akademiakodu.homework.cars2.functions.collision;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.ConnectionControllerSingleton;
import pl.akademiakodu.homework.cars2.functions.Dao;
import pl.akademiakodu.homework.cars2.functions.NoDateException;
import pl.akademiakodu.homework.cars2.functions.StatementViewController;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

public class CollisionAddController {

    @FXML
    private TextField textFieldPlace;
    @FXML
    private TextField textFieldCarId;
    @FXML
    private DatePicker datePickerDate;
    @FXML
    private TextArea textAreaNote;
    @FXML
    private AnchorPane anchorPane;

    private Stage stage;

    public void showCollisionsMenu() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/collision/collision_menu.fxml"));

        try {
            Parent root = fxmlLoader.load();
            CollisionsMenuController controller = fxmlLoader.getController();
            controller.setStage(stage);
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Collision submenu view loading attempt failed.");
        }
    }

    /*
        Make INSERT INTO statement. Displays window with result statement.
     */
    public void addToDataBase() throws IOException {
        String textOfTextFieldCarId = textFieldCarId.getText();
        int carId;
        LocalDate dateCollision = datePickerDate.getValue();
        if (isInteger(textOfTextFieldCarId)) {
            carId = Integer.parseInt(textOfTextFieldCarId);
            Collision collision = new Collision(0, carId, dateCollision, textFieldPlace.getText(), textAreaNote.getText());
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            try {
                root = fxmlLoader.load();
                Scene scene = new Scene(root);
                Stage stageResult = new Stage();
                StatementViewController statementViewController = fxmlLoader.getController();
                statementViewController.setStage(stageResult);
                statementViewController.setAnchorPane(anchorPane);
                stageResult.setScene(scene);
                stageResult.setResizable(false);
                stageResult.setAlwaysOnTop(true);
                try {
                    Dao<Collision> collisionDao = new CollisionDao(ConnectionControllerSingleton.getInstance());
                    collisionDao.add(collision);
                    statementViewController.setLabelStatementText("An attempt to insert data into functions succeed.");

                } catch (SQLException | NoDateException e) {
                    e.printStackTrace();
                    statementViewController.setLabelStatementText("An attempt to insert data into functions failed.");

                } finally {
                    anchorPane.setDisable(true);
                    stageResult.show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("ui/statement_view.fxml"));
            Parent root;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stageResult = new Stage();
            StatementViewController statementViewController = fxmlLoader.getController();
            statementViewController.setStage(stageResult);
            statementViewController.setAnchorPane(anchorPane);
            stageResult.setScene(scene);
            stageResult.setResizable(false);
            stageResult.setAlwaysOnTop(true);
            statementViewController.setLabelStatementText("Car ID is not a integer number.");
            anchorPane.setDisable(true);
            stageResult.show();
        }
    }


    public void setStage(Stage stage) {
        this.stage = stage;
    }


    private boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }
}
