package pl.akademiakodu.homework.cars2.functions.inspection;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.akademiakodu.homework.cars2.functions.car.ShowCarController;

import java.io.IOException;
import java.util.List;

public class InspectionsShowViewController {

    private Stage stage;
    @FXML
    private TextField textFieldId;
    @FXML
    private ListView<Label> listViewInspections;


    public void populateInspectionsList(List<Inspection> inspections) {
        ObservableList<Label> items = FXCollections.observableArrayList();
        if (!inspections.isEmpty()) {
            for (Inspection inspection : inspections) {
                Label item = new Label();
                item.setText(
                        "ID: " + inspection.getId() + "\n" +
                                "Car ID: " + inspection.getCarId() + "\n" +
                                "Date: " + inspection.getDate() + "\n" +
                                "Note: " + inspection.getNote()
                );
                items.add(item);
            }
        } else {
            Label item = new Label();
            item.setPrefWidth(listViewInspections.getPrefWidth());
            item.setPrefHeight(100.0);
            item.setAlignment(Pos.CENTER);
            item.setText(
                    "No inspection."
            );
            items.add(item);
        }

        listViewInspections.setItems(items);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void showCar() {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getClassLoader().getResource("ui/car/show_car.fxml"));
        Parent root;
        try {
            root = fxmlLoader.load();
            ShowCarController showCarController = fxmlLoader.getController();
            showCarController.setStage(stage);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void setTextFieldIdText(String id) {
        textFieldId.setText(id);
    }
}
