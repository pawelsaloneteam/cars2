DROP DATABASE cars2;
CREATE DATABASE cars2;
CREATE TABLE cars2.car (
id int primary key auto_increment,
mark varchar(30),
model varchar(30),
vin varchar(17)
);

CREATE TABLE cars2.collision (
id int primary key auto_increment,
car_id int not null,
date date,
place varchar(200),
note varchar(3000)
);

CREATE TABLE cars2.owner (
id int primary key auto_increment,
surname varchar(100),
name varchar(100)
);

CREATE TABLE cars2.car_owner (
id int primary key auto_increment,
car_id int,
owner_id int,
buy_date date,
sell_date date
);

CREATE TABLE cars2.inspection (
id int primary key auto_increment,
car_id int,
date date,
note text
);

alter table cars2.car_owner add foreign key (car_id) references cars2.car(id);
alter table cars2.car_owner add foreign key (owner_id) references cars2.owner(id);