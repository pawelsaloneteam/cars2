package pl.akademiakodu.homework.cars2;

import pl.akademiakodu.homework.cars2.functions.dao.Dao;
import pl.akademiakodu.homework.cars2.functions.car.Car;
import pl.akademiakodu.homework.cars2.functions.car.CarDao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class UserInterface {
    //    private UserDao userDao;
//    private OwnerDao ownerDao;
//    private InspectionDao inspectionDao;
//    private CollisionDao CollisionDao;
//    private CarOwnerDao carOwnerDao;
    private Connection connection;
    private Scanner input;

    public UserInterface(Connection connection, Scanner input) {
        this.connection = connection;
        this.input = input;
    }

    public void printMainMenu() {
        System.out.println(
                "Main Menu" +
                        "1. Cars\n" +
                        "2. inspection\n" +
                        "3. Owners\n" +
                        "4. Collisions\n" +
                        "\n" +
                        "0. Quit\n" +
                        "\n"
        );

        Optional<Integer> number;

        System.out.println(
                "Enter number:"

        );

        do {
            number = input.hasNextInt() ? Optional.of(input.nextInt()) : Optional.empty();

            while (input.hasNext()) {
                String garbage = input.next();
            }

            if (number.isEmpty()) {
                System.out.println("Wrong input. Enter number listed.");
            } else if (number.get() < 0 || number.get() > 4) {
                System.out.println("Wrong input. Enter number listed.");
                number = Optional.empty();
            }

        } while (number.isEmpty());

        switch (number.get()) {
            case 0:
                return;
            case 1:
                printCarsMenu();
                break;
            case 2:
                printInspectionsMenu();
                break;
            case 3:
                printOwnersMenu();
                break;
            case 4:
                printCollisionsMenu();
                break;
        }

    }

    private void printCarsMenu() {
        System.out.println(
                "Main Menu" +
                        "1. Add\n" +
                        "2. Print All\n" +
                        "3. Print One\n" +
                        "\n" +
                        "0. Go Up\n" +
                        "\n"
        );

        System.out.println(
                "Enter number:"

        );

        Optional<Integer> number;

        do {
            number = input.hasNextInt() ? Optional.of(input.nextInt()) : Optional.empty();

            while (input.hasNext()) {
                String garbage = input.next();
            }

            if (number.isEmpty()) {
                System.out.println("Wrong input. Enter number listed.");
            } else if (number.get() < 0 || number.get() > 3) {
                System.out.println("Wrong input. Enter number listed.");
                number = Optional.empty();
            }

        } while (number.isEmpty());

        switch (number.get()) {
            case 0:
                printMainMenu();
            case 1:
                printAddForm();
                break;
            case 2:
                printAllCars();
                break;
            case 3:
                printOneCar();
                break;
        }
    }

    /*
        Return void.
        Prints one car data. Car is identified by id column value of car table in functions.
        1. Get id from user.
        2. Retrieve data from functions.
        3. Print data.
        4. Print car actions menu.
     */

    private void printOneCar() {
        Optional<Integer> carId;
        System.out.println("Enter car id:");
        do {
            carId = input.hasNextInt() ? Optional.of(input.nextInt()) : Optional.empty();
            while (input.hasNext()) {
                String grabage = input.next();
            }
            if(carId.get() < 0){
                carId = Optional.empty();
            }
            System.out.println("Enter only integer numbers greater or equals to 1.");
        } while (carId.isEmpty());

        CarDao carDao = new CarDao(connection);
        Car car = null;
        try {
            car = carDao.getIdSpecifiedItem(carId.get());
            System.out.println(car.toString() + "\n"); //todo Make car.toString() to print data in possible smallest lines number
            printOneCarMenu();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println( );
            printOneCar();
        }



    }

    /*
        1. Downloads data from functions.
        2. Prints all cars data.
        3. Prints car menu.
     */
    private void printAllCars() {
        Dao carDao = new CarDao(connection);
        List<Car> cars = null;
        try {
            cars = carDao.getAllItemsList();
            for(Car car : cars){
                System.out.println(car.toString());
                System.out.println("----");
            }
            System.out.print("\n\n");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Retrieving data from functions failed.");
        }
        printCarsMenu();
    }

    private void printAddForm() {
        System.out.println("" +
                "Enter vehicle  mark:");
        String mark = input.next();
        while (input.hasNext()) {
            String garbage = input.next();
        }
        System.out.println("Enter vehicel model:");
        String model = input.next();
        while (input.hasNext()) {
            String garbage = input.next();
        }
        System.out.println("Enter VIN:");
        String vin = input.next();
        while (input.hasNext()) {
            String garbage = input.next();
        }

        Car car = new Car(mark, model, vin);

        System.out.println("\n\n");

        System.out.println("Entered data:\n" +
                "\tMark: " + car.getMark() + "\n" +
                "\tModel: " + car.getModel() + "\n" +
                "\tVIN: " + car.getVin() + "\n" +
                "\n");
        System.out.println(
                "1. Add\n" +
                        "\n" +
                        "0. Go Up\n" +
                        "\n"
        );
        System.out.println(
                "Enter number:"
        );
        Optional<Integer> number;
        do {
            number = input.hasNextInt() ? Optional.of(input.nextInt()) : Optional.empty();

            while (input.hasNext()) {
                String garbage = input.next();
            }

            if (number.isEmpty()) {
                System.out.println("Wrong input. Enter number listed.");
            } else if (number.get() < 0 || number.get() > 1) {
                System.out.println("Wrong input. Enter number listed.");
                number = Optional.empty();
            }

        } while (number.isEmpty());

        switch (number.get()) {
            case 0:
                printCarsMenu();
                break;
            case 1:
                Dao carDao = new CarDao(connection);
                try {
                    carDao.add(car);
                    System.out.println("Car was  added to data base.");
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Car adding to data base failed.");
                }
                printCarsMenu();
                break;
        }
    }
}
